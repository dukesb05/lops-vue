/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('projects_types_deploy_config_types', {
    project_type_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'projects_types',
        key: 'id'
      }
    },
    deploy_config_key: {
      type: DataTypes.STRING(255),
      allowNull: false,
      primaryKey: true
    }
  }, {
    tableName: 'projects_types_deploy_config_types'
  });
};
