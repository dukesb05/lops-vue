/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('projects_types', {
    id: {
      type: DataTypes.INTEGER(10),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    composer_cmd: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    requires_local_db: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    excluded_dump_tables: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    post_dump_sql: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: true
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    tableName: 'projects_types'
  });
};
