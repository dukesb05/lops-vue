/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('projects_config_files', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    key: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: true
    }
  }, {
    tableName: 'projects_config_files'
  });
};
