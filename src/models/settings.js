/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('settings', {
    id: {
      type: DataTypes.INTEGER(10),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    project_dir: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    gitlab_token: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: true
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    tableName: 'settings',
    updatedAt: 'updated_at',
    createdAt: 'created_at',
  });
};
