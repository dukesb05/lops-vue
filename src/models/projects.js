/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('projects', {
    id: {
      type: DataTypes.INTEGER(10),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    group_id: {
      type: DataTypes.INTEGER(10),
      allowNull: false,
      references: {
        model: 'projects_groups',
        key: 'id'
      }
    },
    type_id: {
      type: DataTypes.INTEGER(10),
      allowNull: false,
      references: {
        model: 'projects_types',
        key: 'id'
      }
    },
    repo_id: {
      type: DataTypes.INTEGER(10),
      allowNull: false
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    folder: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    repo_url: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    php_version: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    db_environment: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: true
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    tableName: 'projects',
    updatedAt: 'updated_at',
    createdAt: 'created_at',
  });
};
