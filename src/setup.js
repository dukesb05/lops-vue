import { exec, spawn } from "child_process";
import { remote } from "electron";
import request from "request";
import fs from "fs";
import progress from "request-progress";
import steps from "./setup/steps";
import commandExists from "command-exists";
class setup {
    constructor() {
        this.steps = steps;
    }
    downloadInfo() {
        this.installPath = remote.app.getPath('home') + "\\AppData\\Local\\Packages\\Lops";
        this.tarPath = remote.app.getPath('downloads') + "\\";
        this.fileName = "lops.tar";
        this.file = fs.createWriteStream(this.tarPath + this.fileName);
        this.dropBoxUrl = 'https://www.dropbox.com/s/zrvrl0qy0x2bpni/lops.tar?dl=1';
    }
    spawn(command, args) {
        return new Promise((resolve, reject) => {
            this.runSpawn(command, args, resolve, reject);
        });
    }
    runSpawn(command, args, resolve, reject, progressModal) {
        let child;
        child = spawn(
            command, args
        );
        child.stdout.on('data', () => {
            if (progressModal) {
                progressModal.finishProgress();
                progressModal.closeModal();
            }
            resolve(true);
        });
        child.stderr.on('data', () => {
            if (progressModal) {
                progressModal.finishProgress();
                progressModal.closeModal();
            }
            reject(false);
        });
        var error = false;
        child.on('error', () => {
            error = true;
            if (progressModal) {
                progressModal.finishProgress();
                progressModal.closeModal();
            }
            reject(false);
        });
        child.on('close', () => {
            if (!error) {
                resolve(true);
            }
            if (progressModal) {
                progressModal.finishProgress();
                progressModal.closeModal();
            }
        });
        child.stdin.end();
    }
    exec(command) {
        return new Promise((resolve, reject) => {
            this.runExec(command, resolve, reject);
        });
    }
    runExec(command, resolve, reject, progressModal) {
        let child;
        child = exec(
            command
        );
        child.stdout.on("data", function () {
            if (progressModal) {
                progressModal.finishProgress();
                progressModal.closeModal();
            }
            resolve(true);
        });
        var error = false;
        child.stderr.on("data", function (err) {
            if (progressModal) {
                progressModal.finishProgress();
                progressModal.closeModal();
            }
            error = true;
            reject(false);
        });
        child.on("close", function () {
            if (error) {
                reject(false);
            } else {
                resolve(true);
            }
            if (progressModal) {
                progressModal.finishProgress();
                progressModal.closeModal();
            }
        });
        child.stdin.end();
    }
    commandExists(command) {
        return commandExists(command);
    }
    restartComputer() {
        return this.exec("shutdown /r /t 3");
    }
    installNetbeans(progressModal) {
        var downloadPath = remote.app.getPath('downloads') + "\\";
        var fileName = "Netbeans_Setup.exe";
        var file = fs.createWriteStream(downloadPath + fileName);
        var url = 'http://mirrors.koehn.com/apache/netbeans/netbeans/11.1/Apache-NetBeans-11.1-bin-windows-x64.exe';
        const $this = this;
        return new Promise((resolve, reject) => {
            this.installJdk(progressModal).then(() => {
                progressModal.setMessage('Downloading Netbeans Setup File...');
                progressModal.showProgressBar();
                progressModal.showModal();

                progress(request(url)).on('progress', function (state) {
                    progressModal.setProgress(state);
                }).on('error', function (err) {
                    progressModal.setMessage(err);
                }).on('end', function () {
                    progressModal.finishProgress();
                    progressModal.hideProgressBar();
                    progressModal.resetProgress();
                    progressModal.setMessage('Launching Setup for Netbeans...');
                    setTimeout(function () {
                        $this.runExec(file.path, resolve, reject, progressModal);
                    }, 1000);

                }).pipe(file);
            })
        });
    }
    installJdk(progressModal) {
        var downloadPath = remote.app.getPath('downloads') + "\\";
        var fileName = "Java_Jdk_Setup.exe";
        var file = fs.createWriteStream(downloadPath + fileName);
        var url = 'https://download.oracle.com/otn-pub/java/jdk/13+33/5b8a42f3905b406298b72d750b6919f6/jdk-13_windows-x64_bin.exe?AuthParam=1569544352_bd80d2a45df7da1747a6bfb5ce245d42';
        const $this = this;
        return new Promise((resolve, reject) => {
            progressModal.showModal();
            progressModal.setMessage('Checking if JDK is installed...');
            this.isJdkInstalled().then(() => {
                progressModal.setMessage('JDK is installed');
                resolve();
            }).catch(() => {
                progressModal.setMessage('JDK is not installed. Downloading JDK setup file...');
                progressModal.resetProgress();
                progressModal.showProgressBar();

                progress(request(url)).on('progress', function (state) {
                    progressModal.setProgress(state);
                }).on('error', function (err) {
                    progressModal.setMessage(err);
                }).on('end', function () {
                    progressModal.finishProgress();
                    progressModal.hideProgressBar();
                    progressModal.resetProgress();
                    progressModal.setMessage('Launching Setup for JDK...');
                    setTimeout(function () {
                        $this.exec(file.path).then(() => {
                            resolve();
                        });
                    }, 1000);

                }).pipe(file);
            });
        });
    }
    isJdkInstalled() {
        return new Promise((resolve, reject) => {
            if (fs.existsSync("C:\\Program Files\\Java\\jdk-13")) {
                resolve();
            } else {
                reject();
            }
        });
    }
    installAuthy(progressModal) {
        var downloadPath = remote.app.getPath('downloads') + "\\";
        var fileName = "Authy_Setup.exe";
        var file = fs.createWriteStream(downloadPath + fileName);
        var url = 'https://s3.amazonaws.com/authy-electron-repository-production/authy/stable/1.7.0/win32/x64/Authy%20Desktop%20Setup%201.7.0.exe';
        const $this = this;
        return new Promise((resolve, reject) => {
            progressModal.setMessage('Downloading Authy Setup File...');
            progressModal.showProgressBar();
            progressModal.showModal();

            progress(request(url)).on('progress', function (state) {
                progressModal.setProgress(state);
            }).on('error', function (err) {
                progressModal.setMessage(err);
            }).on('end', function () {
                progressModal.finishProgress();
                progressModal.hideProgressBar();
                progressModal.resetProgress();
                progressModal.setMessage('Launching Setup for Authy...');
                setTimeout(function () {
                    $this.runExec(file.path, resolve, reject, progressModal);
                }, 500);

            }).pipe(file);

        });
    }
    installOpenVpn(progressModal) {
        var downloadPath = remote.app.getPath('downloads') + "\\";
        var fileName = "OpenVpn_Setup.exe";
        var file = fs.createWriteStream(downloadPath + fileName);
        var url = 'https://swupdate.openvpn.org/community/releases/openvpn-install-2.4.7-I607-Win10.exe';
        const $this = this;
        return new Promise((resolve, reject) => {
            progressModal.setMessage('Downloading Open VPN Setup File...');
            progressModal.showProgressBar();
            progressModal.showModal();

            progress(request(url)).on('progress', function (state) {
                progressModal.setProgress(state);
            }).on('error', function (err) {
                progressModal.setMessage(err);
            }).on('end', function () {
                progressModal.finishProgress();
                progressModal.hideProgressBar();
                progressModal.resetProgress();
                progressModal.setMessage('Running Setup for Open VPN...');
                setTimeout(function () {
                    $this.runExec(file.path, resolve, reject, progressModal);
                }, 1000);

            }).pipe(file);

        });
    }
    checkDir(dir) {
        return new Promise((resolve, reject) => {
            fs.stat(dir, function (err, stats) {
                if (!err && stats.isDirectory()) {
                    resolve();
                } else {
                    fs.mkdir(dir, function (err) {
                        if (!err) {
                            resolve();
                        }
                    });
                }
            });
        });
    }
    copyVpnConfigFile(vpnConfigFile) {
        return new Promise((resolve, reject) => {
            fs.copyFile(vpnConfigFile.path, "C:\\Program Files\\OpenVPN\\config\\client.ovpn", (err) => {
                if (err) reject();
                resolve();
            });
        });
    }
    installD3Vpn(progressModal, vpnConfigFile) {
        var downloadPath = remote.app.getPath('home') + "\\AppData\\Local\\d3vpn";
        var fileName = "\\d3vpn.exe";
        var url = 'https://www.dropbox.com/s/pyui8b0q2f0wjma/d3vpn.exe?dl=1';
        const $this = this;

        return new Promise((resolve, reject) => {
            $this.checkDir(downloadPath).then(() => {
                progressModal.setMessage('Copying D3 Vpn config file...');
                $this.copyVpnConfigFile(vpnConfigFile).then(() => {
                    progressModal.setMessage('Downloading D3 Vpn Script File...');
                    progressModal.showProgressBar();
                    progressModal.showModal();
                    const options = {
                        url: url,
                        headers: {
                            'User-Agent': 'request'
                        }
                    };
                    progress(request(options)).on('progress', function (state) {
                        progressModal.setProgress(state);
                    }).on('error', function (err) {
                        progressModal.setMessage(err);
                    }).on('end', function () {
                        progressModal.finishProgress();
                        progressModal.hideProgressBar();
                        progressModal.resetProgress();
                        progressModal.closeModal();
                        resolve(true);
                    }).pipe(fs.createWriteStream(downloadPath + fileName));
                });
            });
        });
    }
    installEnvironment(progressModal) {
        this.downloadInfo();
        return new Promise((resolve, reject) => {
            const $this = this;
            progressModal.setMessage('Downloading LOPS WSL Environment...');
            progressModal.showProgressBar();
            progressModal.showModal();
            progress(request($this.dropBoxUrl)).on('progress', function (state) {
                progressModal.setProgress(state);
            }).on('error', function (err) {
                progressModal.setMessage(err);
            }).on('end', function () {
                progressModal.finishProgress();
                progressModal.hideProgressBar();
                progressModal.resetProgress();
                progressModal.setMessage('Installing LOPS WSL Environment...');
                $this.runSpawn("wsl", ["--import", "lops", $this.installPath, ($this.tarPath + $this.fileName)], resolve, reject, progressModal);
            }).pipe($this.file);

        });
    }
    enableWsl() {
        return this.exec("powershell.exe Enable-WindowsOptionalFeature -NoRestart -Online -FeatureName Microsoft-Windows-Subsystem-Linux");
    }
    isSetup() {
        for (const step of Object.keys(this.steps)) {
            this.updateStep(step, 'checking', true);
        }
        const $this = this;
        return new Promise((resolve, reject) => {
            $this.isWslInstalled().then(function () {
                $this.markStepDone('enable_wsl');
                $this.updateStep('enable_wsl', 'checking', false);
                $this.isEnvInstalled().then(function () {
                    $this.markStepDone('install_env')
                    $this.updateStep('install_env', 'checking', false);
                    $this.isNetbeansInstalled().then(() => {
                        $this.markStepDone('install_netbeans')
                        $this.updateStep('install_netbeans', 'checking', false);
                        $this.isVpnInstalled().then(function () {
                            resolve(true);
                        }).catch(function () {
                            reject(false);
                        })
                    }).catch(() => {
                        $this.updateStep('install_netbeans', 'checking', false);
                        $this.isVpnInstalled().then(function () {
                            reject(false);
                        }).catch(function () {
                            reject(false);
                        })
                    });

                }).catch(function () {
                    $this.updateStep('install_env', 'checking', false);
                    reject(false);
                })
            }).catch(function () {
                $this.updateStep('enable_wsl', 'checking', false);
                reject(false);
            })
        });
    }
    isNetbeansInstalled() {
        return new Promise((resolve, reject) => {
            if (fs.existsSync("C:\\Program Files\\NetBeans-11.1\\netbeans\\bin\\netbeans64.exe")) {
                resolve();
            } else {
                reject();
            }
        });
    }
    isVpnInstalled() {
        const $this = this;
        return new Promise((resolve, reject) => {
            require('./services/vpn').default.isNeeded().then(() => {
                $this.isAuthyInstalled().then(function () {
                    $this.markStepDone('install_authy')
                    $this.updateStep('install_authy', 'checking', false);
                    $this.isOpenVpnInstalled().then(function () {
                        $this.markStepDone('install_open_vpn')
                        $this.updateStep('install_open_vpn', 'checking', false);
                        $this.isD3VpnInstalled().then(function () {
                            $this.markStepDone('install_d3vpn_script')
                            $this.updateStep('install_d3vpn_script', 'checking', false);
                            resolve(true);
                        }).catch(function () {
                            $this.updateStep('install_d3vpn_script', 'checking', false);
                            reject(false);
                        })
                    }).catch(function () {
                        $this.updateStep('install_open_vpn', 'checking', false);
                        reject(false);
                    })
                }).catch(function () {
                    $this.updateStep('install_authy', 'checking', false);
                    reject(false);
                })
            }).catch(() => {
                $this.updateStep('install_authy', 'checking', false);
                $this.updateStep('install_open_vpn', 'checking', false);
                $this.updateStep('install_d3vpn_script', 'checking', false);
                $this.markStepDone('install_authy')
                $this.markStepDone('install_open_vpn')
                $this.markStepDone('install_d3vpn_script')
                resolve();
            });
        });
    }
    isD3VpnInstalled() {
        return new Promise((resolve, reject) => {
            require('./services/vpn').default.isNeeded().then(() => {
                if (fs.existsSync(remote.app.getPath('home') + "\\AppData\\Local\\d3vpn\\d3vpn.exe")) {
                    resolve();
                } else {
                    reject();
                }
            }).catch(() => {
                resolve();
            });
        });
    }
    isAuthyInstalled() {
        return new Promise((resolve, reject) => {
            require('./services/vpn').default.isNeeded().then(() => {
                if (fs.existsSync(remote.app.getPath('home') + "\\AppData\\Local\\authy-electron\\Authy Desktop.exe")) {
                    resolve();
                } else {
                    reject();
                }
            }).catch(() => {
                resolve();
            });
        });
    }
    isOpenVpnInstalled() {
        return new Promise((resolve, reject) => {
            require('./services/vpn').default.isNeeded().then(() => {
                if (fs.existsSync("C:\\Program Files\\OpenVPN\\bin\\openvpn-gui.exe")) {
                    resolve();
                } else {
                    reject();
                }
            }).catch(() => {
                resolve();
            });
        });
    }
    isEnvInstalled() {
        return this.exec('wsl --list | findstr "l o p s"');
    }
    isWslInstalled() {
        return this.commandExists("wsl");
    }
    updateStep(step, field, value) {
        this.steps[step][field] = value;
    }
    markStepDone(step) {
        this.steps[step].done = true;
    }
}
export default new setup();