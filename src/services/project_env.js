export default {
    project: false,
    dbDetails: {},
    curEnv: {},
    wpConfig: {},
    configFile(project, type) {
        this.project = project;
        this.curEnv = this.project.deployConfig.environments[project.db_environment];
        this.dbDetails = this.curEnv.db;
        this.wpConfig = this.curEnv.wpconfig ? this.curEnv.wpconfig : {};
        return this.render(this[type]());
    },
    render(template) {
        return template
            .replace(/{DB_NAME}/g, this.dbDetails.name)
            .replace(/{DB_UNAME}/g, this.dbDetails.user)
            .replace(/{DB_PWORD}/g, this.dbDetails.pass)
            .replace(/{DB_HOST}/g, this.dbDetails.host)
            .replace(/{DB_PREFIX}/g, this.dbDetails.prefix)
            .replace(/{CRYPT_KEY}/g, this.curEnv.mageconfig && this.curEnv.mageconfig.crypt_key ? this.curEnv.mageconfig.crypt_key : '25362690901038ea1e9113c70a9f6bc4')
            .replace(/{DOMAIN}/g, this.project.domains[0].get('domain'))
            .replace(/{SALT_AUTH_KEY}/g, this.wpConfig.auth_key)
            .replace(/{SALT_SECURE_AUTH_KEY}/g, this.wpConfig.secure_auth_key)
            .replace(/{SALT_LOGGED_IN_KEY}/g, this.wpConfig.logged_in_key)
            .replace(/{SALT_NONCE_KEY}/g, this.wpConfig.nonce_key)
            .replace(/{SALT_AUTH_SALT}/g, this.wpConfig.auth_salt)
            .replace(/{SALT_SECURE_AUTH_SALT}/g, this.wpConfig.secure_auth_salt)
            .replace(/{SALT_LOGGED_IN_SALT}/g, this.wpConfig.logged_in_salt)
            .replace(/{SALT_NONCE_SALT}/g, this.wpConfig.nonce_salt)
            ;
    },
    wp_php() {
        return `<?php
        define('DB_NAME', '{DB_NAME}');
        define('DB_USER', '{DB_UNAME}');
        define('DB_PASSWORD', '{DB_PWORD}');
        define('DB_HOST', '{DB_HOST}');
        $table_prefix = '{DB_PREFIX}';
        define('WP_ENV', 'development');
        define('WP_HOME', 'https://{DOMAIN}');
        define('WP_SITEURL', 'https://{DOMAIN}');

        /** * This value needs to be defined when on multisite */
        //define('DOMAIN_CURRENT_SITE','example.dev');

        define('FORCE_SSL_LOGIN', false);
        define('FORCE_SSL_ADMIN', false);
        define('DISALLOW_FILE_EDIT', true);
        define('WP_CACHE', false);
        define('WP_DEBUG', false);
        define('WP_POST_REVISIONS', 5);
        define('EMPTY_TRASH_DAYS', 7);
        define('WP_MEMORY_LIMIT', '128M');
        define('CONCATENATE_SCRIPTS', false);
        // generate salts here: https://api.wordpress.org/secret-key/1.1/salt/
        define('AUTH_KEY', '{SALT_AUTH_KEY}');
        define('SECURE_AUTH_KEY', '{SALT_SECURE_AUTH_KEY}');
        define('LOGGED_IN_KEY', '{SALT_LOGGED_IN_KEY}');
        define('NONCE_KEY', '{SALT_NONCE_KEY}');
        define('AUTH_SALT', '{SALT_AUTH_SALT}');
        define('SECURE_AUTH_SALT', '{SALT_SECURE_AUTH_SALT}');
        define('LOGGED_IN_SALT', '{SALT_LOGGED_IN_SALT}');
        define('NONCE_SALT', '{SALT_NONCE_SALT}');
        define('DB_CHARSET', 'utf8');
        define('DB_COLLATE', '');`;
    },
    bs() {
        return `<?php 
            define('DB_NAME', '{DB_NAME}');
            define('DB_UNAME', '{DB_UNAME}');
            define('DB_PWORD', '{DB_PWORD}');
            define('DB_HOST', '{DB_HOST}');
            define('ENV', 'local');
            /*
            define('SMTP_HOST',		'smtp.mailtrap.io');
            define('SMTP_PORT',		'2525');
            define('SMTP_EMAIL',	'info@coastalstylemag.com');
            define('SMTP_UNAME',	'');
            define('SMTP_PWORD',	'');
            define('SMTP_SECURE', false); 
            */`;
    }
};
