import { exec } from "child_process";
import deployConfig from "./deployConfig";
import fs from "fs";
export default {
  php_versions: ['5.6', '7.0', '7.1', '7.2'],
  restartServer() {
    const $this = this;
    return this.exec("service apache2 restart", function (data, resolve) {
      var promises = [];
      $this.php_versions.forEach(version => {
        promises.push($this.exec(`service php${version}-fpm stop ^&^& service php${version}-fpm start`, function (data, presolve) { presolve(data) }));
      });
      Promise.all(promises).then(() => {
        resolve(data);
      }).catch(() => {
      });
    }, function (err, resolve) {
      resolve(err);
    });
  },
  openShell() {
    return new Promise((resolve, reject) => {
      let child;
      child = exec(
        "cmd.exe /c start wsl -d lops"
      );
      child.stdout.on("data", function (data) {
        resolve(data);
      });
      child.stderr.on("data", function (err) {
        if (err.indexOf("No LSB modules") === -1 && err.indexOf("WARNING: apt does not have a stable CLI interface. Use with caution in scripts.") === -1) {
          reject(err);
        }
      });
      child.stdin.end();
    });
  },
  phpVersions() {
    var promises = [];
    this.php_versions.forEach(version => {
      promises.push(this.phpVersionDetails(version));
    });
    return new Promise((resolve, reject) => {
      Promise.all(promises).then(versions => {
        var php_versions = {};
        versions.forEach(version => {
          php_versions[version.version] = version;
        });
        resolve(php_versions);
      }).catch(() => {
        reject();
      });
    });
  },
  phpVersionDetails(version) {
    return new Promise((resolve, reject) => {
      var phpVersion = {};
      phpVersion.version = version;
      this.exec(`service php${version}-fpm status`, function (data, sresolve) {
        if (data.indexOf(`php-fpm${version} is running`) !== -1) {
          sresolve(true);
        } else {
          sresolve(false);
        }
      }).then(fpm_status => {
        phpVersion.fpm_status = fpm_status;
        this.exec(`dpkg --get-selections ^| grep -i php${version}- ^| sed 's/install//' ^| awk '{$1=$1};1'`, function (data, mresolve) {
          mresolve(data.split("\n"));
        }).then(modules => {
          modules = modules.filter(Boolean);
          phpVersion.modules = modules;
          this.exec(`apt search php${version}- ^| grep -vwE \'(` + modules.join('^|') + `)\'`, function (data, amresolve) {
            var mods = data.split("\n");
            var availMods = [];
            for (var i = 0; i < mods.length; i++) {
              if (mods[i].indexOf(`php${version}-`) !== -1 && mods[i].indexOf(`/`) !== -1) {
                var lineSplit = mods[i].split("/");
                if (lineSplit[0] && mods[i + 1]) {
                  availMods.push({ name: lineSplit[0], desc: mods[i + 1].trim() });
                }
              }
            }
            amresolve(availMods);
          }).then(avail_mods => {
            phpVersion.modules_available = avail_mods;
            resolve(phpVersion);
          }).catch(() => {
          });
        }).catch(() => {
        });
      }).catch(() => {
      });
    });
  },
  exec(command, onSuccess, onError, onEnd) {
    return new Promise((resolve, reject) => {
      let child;
      child = exec(
        "wsl -d lops " + command
      );
      child.stdout.on("data", function (data) {
        if (onSuccess) {
          onSuccess(data, resolve, reject);
        } else {
          resolve(data);
        }
      });
      child.stderr.on("data", function (err) {
        if (err.indexOf("No LSB modules") === -1 && err.indexOf("WARNING: apt does not have a stable CLI interface. Use with caution in scripts.") === -1) {
          if (onError) {
            onError(err, resolve, reject);
          } else {
            reject(err);
          }

        }
      });
      child.on("close", function () {
        if (onEnd) {
          onEnd(resolve, reject);
        } else {
          reject();
        }
      });
      child.stdin.end();
    });
  },
  execWithProgress(command, progressModal) {
    return new Promise((resolve, reject) => {
      let child;
      child = exec(
        "wsl -d lops " + command
      );
      child.stdout.on("data", function (data) {
        progressModal.appendConsoleText(data);
      });
      child.stderr.on("data", function (err) {
        if (err.indexOf("No LSB modules") === -1 && err.indexOf("WARNING: apt does not have a stable CLI interface. Use with caution in scripts.") === -1) {
          progressModal.appendConsoleText(err);
        }
      });
      child.on("close", function () {
        resolve();
      });
      child.stdin.end();
    });
  },
  dist() {
    return this.exec("lsb_release -a", function (data, resolve) {
      var releaseInfo = data.split("\n");
      var release = {};
      release.dist_id = releaseInfo[0].replace(/Distributor ID:	/, '');
      release.dist_desc = releaseInfo[1].replace(/Description:	/, '');
      release.dist_release = releaseInfo[2].replace(/Release:	/, '');
      release.dist_codename = releaseInfo[3].replace(/Codename:	/, '');
      resolve(release);
    });
  },
  apacheStatus() {
    return this.exec("service apache2 status", function (data, resolve) {
      var status = {};
      if (data.indexOf("apache2 is running") !== -1) {
        status.apache_running = true;
      } else {
        status.apache_running = false;
      }
      resolve(status);
    });
  },
  apacheDetails() {
    return this.exec("apache2 -v", function (data, resolve) {
      var info = data.split("\n");
      var details = {};
      details.apache_version = info[0].replace(/Server version: /, '');
      details.apache_build_date = info[1].replace(/Server built:   /, '');
      resolve(details);
    });
  },
  projectDir(settings, project) {
    var project_dir = settings.project_dir.replace(/\\/g, '/').replace(/C\:/, '/mnt/c');
    var dir =
      project_dir +
      "/" +
      project.group.get("folder") +
      "/" +
      project.folder;
    return dir;
  },
  winProjectDir(settings, project) {
    var project_dir = settings.project_dir;
    var dir =
      project_dir +
      "\\" +
      project.group.get("folder") +
      "\\" +
      project.folder;
    return dir;
  },
  cloneProject(settings, project, progressModal) {
    var dir = this.projectDir(settings, project);
    return this.execWithProgress(`git clone ${project.repo_url} ${dir}`, progressModal);
  },
  updateProjectComposer(settings, project, progressModal) {
    const dir = this.projectDir(settings, project);
    const composer_cmd = project.type.get('composer_cmd');
    const php_version = project.php_version;
    return this.execWithProgress(`cd ${dir} ^&^& /usr/bin/php${php_version} /usr/bin/${composer_cmd}`, progressModal);
  },
  winHostsDomainExists(domain) {
    return new Promise((resolve, reject) => {
      let child;
      child = exec(
        `findstr ${domain} C:\\Windows\\System32\\drivers\\etc\\hosts`
      );
      child.stdout.on("data", function () {
        resolve();
      });
      child.stderr.on("data", function () {
        reject();
      });
      child.on("close", function () {
        reject();
      });
      child.stdin.end();
    });
  },
  domainExists(domain) {
    return this.exec(`grep ${domain} /etc/hosts`);
  },
  addDomain(domain) {
    const $this = this;
    return new Promise((resolve, reject) => {
      $this.domainExists(domain).then(() => {
        resolve();
      }).catch(() => {
        $this.exec(`echo "127.0.0.1 ${domain}"^>^>/etc/hosts`, false, false, function (adresolve, adreject) {
          adresolve();
        }).then(() => {
          resolve();
        });
      });
    });
  },
  winHostsDomainExists(domain) {
    return new Promise((resolve, reject) => {
      let child;
      child = exec(
        `findstr ${domain} C:\\Windows\\System32\\drivers\\etc\\hosts`
      );
      child.stdout.on("data", function () {
        resolve();
      });
      child.stderr.on("data", function () {
        reject();
      });
      child.on("close", function () {
        reject();
      });
      child.stdin.end();
    });
  },
  addWinHostsDomain(domain) {
    return new Promise((resolve, reject) => {
      let child;
      child = exec(
        `echo 127.0.0.1 ${domain}>>C:\\Windows\\System32\\drivers\\etc\\hosts`
      );
      child.stdout.on("data", function () {
        resolve();
      });
      child.stderr.on("data", function () {
        reject();
      });
      child.on("close", function () {
        resolve();
      });
      child.stdin.end();
    });
  },
  createApacheFile(project) {
    const $this = this;
    return new Promise((resolve, reject) => {
      const apacheFile = this.renderApacheFile(project);
      fs.writeFile(project.directory + "/lopsapacheconf.conf", apacheFile, function (err) {
        if (err) reject();
        const wslDir = project.directory.replace(/\\/g, '/').replace(/C\:/, '/mnt/c');
        $this.exec(`cp ${wslDir}/lopsapacheconf.conf /etc/apache2/sites-available/project.${project.id}.conf ^&^& rm ${wslDir}/lopsapacheconf.conf`, false, false, function (adresolve, adreject) {
          adresolve();
        }).then(() => {
          resolve();
        });
      });
    });
  },
  renderApacheFile(project) {
    const projectDir = project.web_directory.replace(/\\/g, '/').replace(/C\:/, '/mnt/c');
    var apacheFile = '';
    var ending = "\n";
    project.domains.forEach(domain => {
      apacheFile += `<VirtualHost ${domain.get('domain')}:80>${ending}`;
      apacheFile += `DocumentRoot ${projectDir}${ending}`;
      apacheFile += `ErrorLog \${APACHE_LOG_DIR}/project.${project.id}.error.log${ending}`;
      apacheFile += `<FilesMatch \.php$>${ending}`;
      apacheFile += `SetHandler "proxy:unix:/var/run/php/php${project.php_version}-fpm.sock|fcgi://localhost/"${ending}`;
      apacheFile += `</FilesMatch>${ending}`;
      apacheFile += `</VirtualHost>${ending}`;
      apacheFile += `<IfModule mod_ssl.c>${ending}`;
      apacheFile += `<VirtualHost ${domain.get('domain')}:443>${ending}`;
      apacheFile += `DocumentRoot ${projectDir}${ending}`;
      apacheFile += `SSLEngine on${ending}`;
      apacheFile += `SSLCertificateFile /etc/ssl/certs/apache-selfsigned.crt${ending}`;
      apacheFile += `SSLCertificateKeyFile /etc/ssl/private/apache-selfsigned.key${ending}`;
      apacheFile += `ErrorLog \${APACHE_LOG_DIR}/project.${project.id}.error.log${ending}`;
      apacheFile += `<FilesMatch \.php$>${ending}`;
      apacheFile += `SetHandler "proxy:unix:/var/run/php/php${project.php_version}-fpm.sock|fcgi://localhost/"${ending}`;
      apacheFile += `</FilesMatch>${ending}`;
      apacheFile += `</VirtualHost>${ending}`;
      apacheFile += `</IfModule>${ending}`;
    });
    return apacheFile;
  },
  enableProjectApacheConf(project){
    return this.exec(`a2ensite project.${project.id} ^&^& service apache2 reload`);
  }
};
