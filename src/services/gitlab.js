import { exec } from "child_process";
import { Gitlab } from 'gitlab';
import Vue from "vue";
const url = "https://git.d3corp.com";
export default {
  api_key: false,
  apiConnection: false,
  apiKey() {
    const $this = this;
    return new Promise((resolve, reject) => {
      if (!$this.api_key) {
        Vue.prototype.$db.settings.then(settings => {
          settings = settings[0].get();
          if (settings.gitlab_token) {
            $this.api_key = settings.gitlab_token;
            resolve($this.api_key);
          } else {
            reject();
          }
        });
      } else {
        resolve($this.api_key);
      }
    });
  },
  api() {
    const $this = this;
    return new Promise((resolve, reject) => {
      if ($this.apiConnection) {
        resolve($this.apiConnection);
      } else {
        if (!$this.api_key) {
          $this.apiKey().then(key => {
            $this.apiConnection = new Gitlab({
              host: url,
              token: key
            });
            resolve($this.apiConnection);
          }).catch((e) => {
            console.error(e);
            reject();
          });
        } else {
          $this.apiConnection = new Gitlab({
            host: url,
            token: $this.api_key
          });
          resolve($this.apiConnection);
        }
      }
    });
  },
  apiConnected() {
    const $this = this;
    return new Promise((resolve) => {
      $this.api().then(() => {
        resolve(true);
      }).catch(() => {
        resolve(false);
      });
    });
  },
  search(term) {
    const $this = this;
    return new Promise((resolve, reject) => {
      $this.api().then((api) => {
        api.Projects.search(term).then(projects => {
          for (var i = 0; i < projects.length; i++) {
            var p_name = projects[i].path;
            p_name = p_name.replace(/-/g, ' ').replace(/site/i, '').replace(/^(.)|\s+(.)/g, function ($1) {
              return $1.toUpperCase()
            }).trim();
            projects[i].p_name = p_name;
            projects[i].domain = p_name.replace(/ /g, '').toLowerCase() + '.test';
            projects[i].folder = projects[i].path;
          }
          resolve(projects);
        });
      }).catch((e) => {
        reject(e);
      });
    });
  },
  sshConnected() {
    return new Promise((resolve) => {

      let child;
      child = exec(
        `wsl -d lops ssh git@git.d3corp.com`
      );
      child.stdout.on("data", function (data) {
        if (data.indexOf("Welcome to GitLab") !== -1) {
          resolve(true);
        }
        if (data.indexOf("Resource temporarily unavailable") !== -1) {
          resolve(false);
        }
      });
      child.stderr.on("data", function (err) {
        if (err.indexOf("Pseudo-terminal will not be allocated") === -1) {
          resolve(false);
        }
      });
      child.stdin.end();
    });
  },
  sshKey() {
    return new Promise((resolve, reject) => {
      let child;
      child = exec(
        "wsl -d lops cat /root/.ssh/id_rsa.pub"
      );
      child.stdout.on("data", function (sshKey) {
        resolve(sshKey);
      });
      child.stderr.on("data", function () {
        reject(false);
      });
      child.stdin.end();
    });
  },

};
