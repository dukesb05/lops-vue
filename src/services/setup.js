// create an axios instance with default options
export default {
  async isSetup() {
    var { spawn } = require("child_process"),
      child;
    child = spawn("powershell.exe", [
      "Get-WindowsOptionalFeature",
      "-Online",
      "-FeatureName",
      "*Microsoft-Windows-Subsystem-Linux*"
    ]);
    child.stdout.on("data", function(data) {
      console.log("Powershell Data: " + data);
    });
    child.stderr.on("data", function(data) {
      console.log("Powershell Errors: " + data);
    });
    child.on("exit", function() {
      console.log("Powershell Script finished");
    });
    child.stdin.end(); //end input
  }
};
