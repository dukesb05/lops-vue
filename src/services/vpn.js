import superagent from "superagent";
import { exec } from "child_process";
import { remote } from "electron";
export default {
  vpn_ip: "52.205.141.174",
  office_ip: "74.93.195.209",
  //office_ip: "69.251.2.57",
  isConnected() {
    const $this = this;
    return new Promise((resolve, reject) => {
      this.currentIp().then((ip) => {
        if (ip == $this.vpn_ip) {
          resolve();
        } else {
          reject();
        }
      }).catch(() => {
        reject();
      })
    });
  },
  currentIp() {
    return new Promise((resolve, reject) => {
      superagent
        .get('http://ipecho.net/plain')
        .end(function (err, res) {
          if (err) {
            reject(err);
          }
          resolve(res.text);
        });
    });
  },
  isNeeded() {
    const $this = this;
    return new Promise((resolve, reject) => {
      this.currentIp().then(function (ip) {
        if (ip == $this.office_ip) {
          reject();
        } else {
          resolve();
        }
      }).catch(function () {
        resolve();
      })
    });
  },
  connect() {
    return new Promise((resolve, reject) => {
      let child;
      child = exec(
        remote.app.getPath('home') + "\\AppData\\Local\\d3vpn\\d3vpn.exe"
      );
      child.stdout.on("data", function () {
        resolve();
      });
      var error = false;
      child.stderr.on("data", function (err) {
        error = true;
        reject();
      });
      child.on("close", function () {
        if (error) {
          reject();
        } else {
          resolve();
        }
      });
      child.stdin.end();
    });
  },
  disconnect() {
    return new Promise((resolve, reject) => {
      let child;
      child = exec(
        `"C:\\Program Files\\OpenVPN\\bin\\openvpn-gui.exe" --command disconnect client`
      );
      child.stdout.on("data", function () {
        resolve();
      });
      var error = false;
      child.stderr.on("data", function (err) {
        error = true;
        reject();
      });
      child.on("close", function () {
        if (error) {
          reject();
        } else {
          resolve();
        }
      });
      child.stdin.end();
    });
  }
};
