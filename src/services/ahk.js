import { exec } from "child_process";
import { remote } from "electron";

export default {
  run(script) {
    return new Promise((resolve, reject) => {
      const scriptData = this[script + "Script"]();
      console.log(`echo '${scriptData}' | "C:\\Program Files\\AutoHotkey\\AutoHotkey.exe"`);
      return;
      let child;
      child = exec(
        `echo "${scriptData}" || C:\\Program Files\\AutoHotkey\\AutoHotkey.exe`
      );
      child.stdout.on("data", function () {
        resolve();
      });
      child.stderr.on("data", function (err) {
        if (err.indexOf("No LSB modules") === -1 && err.indexOf("WARNING: apt does not have a stable CLI interface. Use with caution in scripts.") === -1) {
          reject(err);
        }
      });
      child.stdin.end();
    });
  },
  vpn_connectScript() {
    const authyPath = remote.app.getPath('home') + "\\AppData\\Local\\authy-electron\\Authy Desktop.exe";
    return '#SingleInstance force\\nif WinExist("Authy")\\n    WinClose ; use the window found above\\nRun, "' + authyPath + '"\\nSleep, 4000\\nControlClick, x182 y113, Authy\\nSleep, 500\\nControlClick, x350 y550, Authy\\nSleep, 1000\\nWinClose, Authy\\nRun, "C:\\Program Files\\OpenVPN\\bin\\openvpn-gui.exe" --command connect client\\nSleep, 1000\\nControlClick, x156 y128, client\\nSleep, 50\\nSend ^v\\nSleep, 100\\nControlClick, OK, client';
  }
};
