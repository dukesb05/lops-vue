import gitlab from './gitlab';
import jsyaml from "js-yaml";
import http from "http";
import https from "https";
export default {
  get(path) {
    var $this = this;
    return new Promise((resolve, reject) => {
      gitlab.api().then(api => {
        api.RepositoryFiles.show(237, path + "/_config.yml", 'master').then(file => {
          var doc = jsyaml.load(atob(file.content));
          doc.environments = {};
          api.Repositories.tree(237, { path: path + "/environments" }).then(envs => {
            var promises = [];
            envs.forEach(env => {
              promises.push(api.RepositoryFiles.show(237, `${path}/environments/${env.name}`, 'master'));
            });
            Promise.all(promises).then(environments => {
              var phpVersionPromises = [];
              const pathSplit = path.split('/');
              const gitGroup = pathSplit[0];
              const gitProject = pathSplit[1];
              environments.forEach(environment => {
                var env_yml = jsyaml.load(atob(environment.content));
                const envName = environment.file_name.replace(/\.yml/, '');
                doc.environments[envName] = env_yml;
                var url = env_yml.server.home_url.replace(/{{ git_project }}/g, gitProject).replace(/{{ git_group }}/g, gitGroup);
                phpVersionPromises.push($this.getPhpVersion(url, envName));
              });
              Promise.all(phpVersionPromises).then(phpVersions => {
                phpVersions.forEach(phpVersion => {
                  doc.environments[phpVersion.env].server.php_version_full = phpVersion.version;
                  doc.environments[phpVersion.env].server.php_version = phpVersion.two_digit_version;
                });
                resolve(doc);
              });

            });
          });
        }).catch(e => {
          reject(e);
        })
      }).catch(() => {
      });
    });
  },
  getPhpVersion(url, envName) {
    return new Promise((resolve, reject) => {
      var lib = url.indexOf('https://') !== -1 ? https : http;
      var port = url.indexOf('https://') !== -1 ? 443 : 80;
      var host = url.replace(/https:\/\//g, '').replace(/http:\/\//g, '');
      var options = { method: 'HEAD', host: host, port: port, path: '/', auth: 'd3:d3' };
      var req = lib.request(options, function (res) {
        const poweredBy = res.headers['x-powered-by'];
        var phpVersion = "5.6";
        if (poweredBy) {
          phpVersion = poweredBy.replace(/PHP\//g, '');
        }
        const versionSplit = phpVersion.split('.');
        const phpVersionTwoDigit = versionSplit[0] + '.' + versionSplit[1];
        resolve({ version: phpVersion, two_digit_version: phpVersionTwoDigit, env: envName });
      }
      );
      req.end();
    });

  }
};
