export default {
    propertiesFile(project) {
        return `auxiliary.org-netbeans-modules-css-prep.less_2e_configured=true
include.path=\${php.global.include.path}
php.version=PHP_${project.php_version.replace(/\./g, '')}
source.encoding=UTF-8
src.dir=.
tags.asp=false
tags.short=true
web.root=.`;
    },
    projectFile(project) {
        return `<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://www.netbeans.org/ns/project/1">
    <type>org.netbeans.modules.php.project</type>
    <configuration>
        <data xmlns="http://www.netbeans.org/ns/php-project/1">
            <name>${project.name}</name>
        </data>
    </configuration>
</project>`;
    },
    privatePropertiesFile(project) {
        return `copy.src.files=false
copy.src.on.open=false
copy.src.target=
index.file=index.php
url=https://${project.domains[0].get('domain')}/`;
    },
    privateFile() {
        return `<?xml version="1.0" encoding="UTF-8"?>
<project-private xmlns="http://www.netbeans.org/ns/project-private/1">
    <editor-bookmarks xmlns="http://www.netbeans.org/ns/editor-bookmarks/2" lastBookmarkId="0"/>
    <open-files xmlns="http://www.netbeans.org/ns/projectui-open-files/2">
        <group/>    
    </open-files>
</project-private>`;
    }
};
