import axios from "axios";
import { loadProgressBar } from "axios-progress-bar";
// create an axios instance with default options
const http = axios.create({ baseURL: "http://lopsapi.test/" });
loadProgressBar({}, http);
export default {
  async fetchProjects() {
    try {
      const result = await http.get("projects");
      return result.data;
    } catch (e) {
      console.log(e);
    }
  },
  async fetchProject(id) {
    try {
      const result = await http.get(`projects/${id}`);
      return result.data;
    } catch (e) {
      console.log(e);
    }
  },
  async handleAction(id, action) {
    try {
      const result = await http.post(`projects/${id}/action/${action}`);
      return result.data;
    } catch (e) {
      console.log(e);
    }
  },
  async vpnStatus() {
    try {
      const result = await http.get(`vpn/status`);
      return result.data;
    } catch (e) {
      console.log(e);
    }
  },
  async toggleVpn() {
    try {
      const result = await http.get(`vpn/toggle`);
      return result.data;
    } catch (e) {
      console.log(e);
    }
  },
  async fetchServerDetails() {
    try {
      const result = await http.get(`server/details`);
      return result.data;
    } catch (e) {
      console.log(e);
    }
  },
  async updateDomain(id, domain) {
    try {
      const result = await http.post(`projects/${id}/updateDomain`, {
        domain: domain
      });
      return result.data;
    } catch (e) {
      console.log(e);
    }
  },
  async updatePhpVersion(id, version) {
    try {
      const result = await http.post(`projects/${id}/updatePhpVersion`, {
        php_version: version
      });
      return result.data;
    } catch (e) {
      console.log(e);
    }
  },
  async updateDbEnvironment(id, environment) {
    try {
      const result = await http.post(`projects/${id}/updateDbEnvironment`, {
        environment: environment
      });
      return result.data;
    } catch (e) {
      console.log(e);
    }
  },
  async restartServer() {
    try {
      const result = await http.post(`server/restart`);
      return result.data;
    } catch (e) {
      console.log(e);
    }
  },
  async openServerTerminal() {
    try {
      const result = await http.post(`server/terminal`);
      return result.data;
    } catch (e) {
      console.log(e);
    }
  },
  async installPhpModule(version, php_mod) {
    try {
      const result = await http.post(`server/installPhpModule`, {
        php_version: version,
        module: php_mod
      });
      return result.data;
    } catch (e) {
      console.log(e);
    }
  },
  async fetchSettings() {
    try {
      const result = await http.get(`settings`);
      return result.data;
    } catch (e) {
      console.log(e);
    }
  },
  async updateRootProjectDirectory(directory) {
    try {
      const result = await http.post(`settings/updateProjectDirectory`, {
        directory: directory
      });
      return result.data;
    } catch (e) {
      console.log(e);
    }
  },
  async updateGitlabToken(token) {
    try {
      const result = await http.post(`settings/updateGitlabToken`, {
        token: token
      });
      return result.data;
    } catch (e) {
      console.log(e);
    }
  },
  async runProjectSetup(id, action) {
    try {
      const result = await http.post(`projects/${id}/setup/${action}`);
      return result.data;
    } catch (e) {
      console.log(e);
    }
  },
  async gitlabSearch(term) {
    try {
      const result = await http.get(`gitlab/search?term=${term}`);
      return result.data;
    } catch (e) {
      console.log(e);
    }
  },
  async saveProject(data) {
    try {
      const result = await http.post(`projects/create`, data);
      return result.data;
    } catch (e) {
      console.log(e);
    }
  }
};
