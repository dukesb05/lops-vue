

import AppLayout from "../layout/App.vue";
import SetupLayout from "../layout/Setup.vue";

// GeneralViews
import NotFound from "../pages/NotFoundPage.vue";

// Admin pages
import Projects from "src/pages/Projects.vue";
import Project from "src/pages/Project.vue";
import LopsServer from "src/pages/Server.vue";
import Settings from "src/pages/Settings.vue";
import setup from "src/setup";
let redirect = '';
const routes = [
  {
    path: "/",
    component: AppLayout,
    redirect: "/projects",
    children: [
      {
        path: "projects",
        name: "Projects",
        component: Projects
      },
      {
        path: "project/:id",
        name: "Project",
        component: Project
      },
      {
        path: "server",
        name: "Server",
        component: LopsServer
      },
      {
        path: "settings",
        name: "Settings",
        component: Settings
      }
    ]
  },
  {
    path: "/setup",
    component: SetupLayout,
    name: "Setup"
  },
  { path: "*", component: NotFound }
];
export default routes;
