import GlobalComponents from "./globalComponents";
import SideBar from "./components/SidebarPlugin";
import Vuex from "vuex";
import VueSweetalert2 from 'vue-sweetalert2';
import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import { faGitlab } from "@fortawesome/free-brands-svg-icons";
import Sequelize from "sequelize";
import {
  faCube,
  faFolder,
  faWrench,
  faTerminal,
  faServer,
  faTrash,
  faPowerOff,
  faSearch,
  faCog,
  faGlobe,
  faEye,
  faHeart,
  faCheckCircle,
  faTimesCircle,
  faPlay,
  faSync,
  faDatabase,
  faCopy,
  faCheck,
  faSpinner
} from "@fortawesome/free-solid-svg-icons";

// asset imports
import "bootstrap/scss/bootstrap.scss";
import "./assets/sass/light-bootstrap-dashboard.scss";
library.add(
  faGitlab,
  faCube,
  faFolder,
  faWrench,
  faTerminal,
  faServer,
  faTrash,
  faPowerOff,
  faSearch,
  faCog,
  faGlobe,
  faEye,
  faHeart,
  faPlay,
  faSync,
  faCheckCircle,
  faTimesCircle,
  faDatabase,
  faCopy,
  faCheck,
  faSpinner
);
let db = {};
/**
 * This is the main plugin where plugins are registerd.
 */
export default {
  install(Vue) {
    Vue.use(GlobalComponents);
    Vue.use(SideBar);
    Vue.use(Vuex);
    Vue.use(VueSweetalert2);
    const sequelize = new Sequelize({
      dialect: 'sqlite',
      storage: './lops.sqlite3',
      logging: false
    });
    const models = [
      require('./models/migrations')(sequelize, Sequelize),
      require('./models/projects_config_files_build_tasks')(sequelize, Sequelize),
      require('./models/projects_config_files')(sequelize, Sequelize),
      require('./models/projects_domains')(sequelize, Sequelize),
      require('./models/projects_groups')(sequelize, Sequelize),
      require('./models/projects')(sequelize, Sequelize),
      require('./models/projects_types')(sequelize, Sequelize),
      require('./models/projects_types_deploy_config_types')(sequelize, Sequelize),
      require('./models/settings')(sequelize, Sequelize),
    ];

    models.forEach(model => {
      db[model.name] = model;
    });
    db.projects_types.hasMany(db.projects_types_deploy_config_types, { foreignKey: 'project_type_id', sourceKey: 'id' });
    db.projects_types_deploy_config_types.belongsTo(db.projects_types, { foreignKey: 'project_type_id', targetKey: 'id' });
    db.projects.hasMany(db.projects_domains, { foreignKey: 'project_id', sourceKey: 'id', as: "domains" });
    db.projects_domains.belongsTo(db.projects, { foreignKey: 'project_id', targetKey: 'id' });
    db.projects_types.hasMany(db.projects, { foreignKey: 'type_id', sourceKey: 'id' });
    db.projects.belongsTo(db.projects_types, { foreignKey: 'type_id', targetKey: 'id', as: 'type' });
    db.projects_groups.hasMany(db.projects, { foreignKey: 'group_id', sourceKey: 'id' });
    db.projects.belongsTo(db.projects_groups, { foreignKey: 'group_id', targetKey: 'id', as: 'group' });
    db.projects_config_files.hasMany(db.projects_config_files_build_tasks, { foreignKey: 'config_file_id', sourceKey: 'id' });
    db.projects_config_files_build_tasks.belongsTo(db.projects_config_files, { foreignKey: 'config_file_id', targetKey: 'id' });
    db.settings.sync();
    db.settings = db.settings.findOrCreate({ where: { id: 1 } });
    db.all_project_types = db.projects_types.findAll({
      include: [
        { model: db.projects_types_deploy_config_types, as: db.projects_types_deploy_config_types.tableName }
      ]
    });
    db.all_project_config_files = db.projects_config_files.findAll({
      include: [
        { model: db.projects_config_files_build_tasks, as: db.projects_config_files_build_tasks.tableName }
      ]
    });
    Vue.prototype.$db = db;
    this.runMigrations(db, models);
    Vue.component("fa-icon", FontAwesomeIcon);
  },
  runMigrations(db, models) {
    //var setup = require('./migrations/setup').default;
    //setup.run(db, models);
    var migrations = [];
    var migrationsToDo = [];
    db.migrations.findAll().then(db_migrations => {
      if (migrations) {
        migrations.forEach(migration => {
          var migrationFound = false;
          db_migrations.forEach(dbmigration => {
            if (dbmigration.get('migration') == migration) {
              migrationFound = true;
            }
          });
          if (!migrationFound) {
            migrationsToDo.push(migration);
          }
        });
      }
    }).catch(() => {
      var setup = require('./migrations/setup').default;
      setup.run(db, models);
    })
  }
};
