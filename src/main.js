import Vue from "vue";
import VueRouter from "vue-router";
import AppVue from "./App.vue";

import AppPlugin from "./app";

// router setup
import routes from "./routes/routes";

import "axios-progress-bar/dist/nprogress.css";
//store
import store from "./store";
import setup from "./setup";

// plugin setup
Vue.use(VueRouter);
Vue.use(AppPlugin);

// configure router
const router = new VueRouter({
  routes, // short for routes: routes
  linkActiveClass: "nav-item active",
  scrollBehavior: to => {
    if (to.hash) {
      return { selector: to.hash };
    } else {
      return { x: 0, y: 0 };
    }
  }
});

setup.isSetup().catch(() => {
  if (router.history.current.path != '/setup') {
    router.push('/setup')
  }
})
/* eslint-disable no-new */
new Vue({
  el: "#app",
  store,
  render: h => h(AppVue),
  router
});
