import Sidebar from './SideBar.vue'
import SidebarLink from './SidebarLink.vue'


const SidebarPlugin = {

  install (Vue) {
    Object.defineProperty(Vue.prototype, '$sidebar', {
      get () {
        return this.$store.state.sidebar;
      }
    })
    Vue.component('side-bar', Sidebar)
    Vue.component('sidebar-link', SidebarLink)
  }
}

export default SidebarPlugin
