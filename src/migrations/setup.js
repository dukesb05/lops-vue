export default {
    run(db, models) {
        var promises = [];
        models.forEach(model => {
            if (typeof db[model.name] === 'function') {
                promises.push(db[model.name].sync({ force: true }));
            }
        });
        db.projects_types.hasMany(db.projects_types_deploy_config_types, { foreignKey: 'project_type_id', sourceKey: 'id' });
        db.projects_types_deploy_config_types.belongsTo(db.projects_types, { foreignKey: 'project_type_id', targetKey: 'id' });
        db.projects.hasMany(db.projects_domains, { foreignKey: 'project_id', sourceKey: 'id', as: "domains" });
        db.projects_domains.belongsTo(db.projects, { foreignKey: 'project_id', targetKey: 'id' });
        db.projects_types.hasMany(db.projects, { foreignKey: 'type_id', sourceKey: 'id' });
        db.projects.belongsTo(db.projects_types, { foreignKey: 'type_id', targetKey: 'id', as: "type" });
        db.projects_groups.hasMany(db.projects, { foreignKey: 'group_id', sourceKey: 'id' });
        db.projects.belongsTo(db.projects_groups, { foreignKey: 'group_id', targetKey: 'id', as: 'group' });
        db.projects_config_files.hasMany(db.projects_config_files_build_tasks, { foreignKey: 'config_file_id', sourceKey: 'id' });
        db.projects_config_files_build_tasks.belongsTo(db.projects_config_files, { foreignKey: 'config_file_id', targetKey: 'id' });
        Promise.all(promises).then(() => {
            db.projects_types.bulkCreate([
                { id: 1, name: 'Basesite', composer_cmd: 'composer update', requires_local_db: 0, excluded_dump_tables: 'log_*', post_dump_sql: null },
                { id: 2, name: 'Wordpress', composer_cmd: 'composer update', requires_local_db: 0, excluded_dump_tables: null, post_dump_sql: null },
                { id: 3, name: 'Magento', composer_cmd: 'composer update && composer run-script post-install-cmd -vvv -- --redeploy', requires_local_db: 1, excluded_dump_tables: 'log_*', post_dump_sql: "UPDATE {prefix}core_config_data SET value = '{siteurl}' WHERE path IN ('web/unsecure/base_url','web/secure/base_url');SET FOREIGN_KEY_CHECKS=0;UPDATE {prefix}core_store SET store_id = 0 WHERE code='admin';UPDATE {prefix}core_store_group SET group_id = 0 WHERE name='Default';UPDATE {prefix}core_website SET website_id = 0 WHERE code='admin';UPDATE {prefix}customer_group SET customer_group_id = 0 WHERE customer_group_code='NOT LOGGED IN';SET FOREIGN_KEY_CHECKS=1;REPLACE INTO `{prefix}core_config_data`(`path`,`value`) VALUES ('dev/template/allow_symlink', 1);" }
            ]).then(() => {
                db.projects_types_deploy_config_types.bulkCreate([
                    { project_type_id: 1, deploy_config_key: 'basesite' },
                    { project_type_id: 1, deploy_config_key: 'basesite-4' },
                    { project_type_id: 1, deploy_config_key: 'basesite-5.0' },
                    { project_type_id: 1, deploy_config_key: 'basesite-5.2' },
                    { project_type_id: 1, deploy_config_key: 'general' },
                    { project_type_id: 1, deploy_config_key: 'zero' },
                    { project_type_id: 2, deploy_config_key: 'wordpress' },
                    { project_type_id: 2, deploy_config_key: 'wordpress-bedrock' },
                    { project_type_id: 2, deploy_config_key: 'wordpress-legacy' },
                    { project_type_id: 3, deploy_config_key: 'magento' },
                    { project_type_id: 3, deploy_config_key: 'magento-legacy' },
                ]).then(() => {

                }).catch(() => {
                });
            }).catch(() => {
            });
            db.projects_config_files.bulkCreate([
                { id: 1, key: 'bs', name: 'Basesite' },
                { id: 2, key: 'mag', name: 'Magento' },
                { id: 3, key: 'wp_env', name: 'Wordpress .env' },
                { id: 4, key: 'wp_php', name: 'Wordpress .php' }
            ]).then(() => {
                db.projects_config_files_build_tasks.bulkCreate([
                    { config_file_id: 1, build_task: "basesite-config" },
                    { config_file_id: 1, build_task: "basesite52-config" },
                    { config_file_id: 1, build_task: "legacy-base-site-config" },
                    { config_file_id: 1, build_task: "shorerent-config" },
                    { config_file_id: 2, build_task: "magento-config" },
                    { config_file_id: 3, build_task: "bedrock-config" },
                    { config_file_id: 4, build_task: "legacy-wp-config" },
                    { config_file_id: 4, build_task: "legacy-wp-config-oceanpines" },
                ]).then(() => { }).catch(() => {
                });
            }).catch(() => {
            });
        })

    }
}