// initial state
const state = {
  showSidebar: false
};

// getters
const getters = {};

// actions
const actions = {};

// mutations
const mutations = {
  toggleSidebar(state) {
    state.showSidebar = !state.showSidebar;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
