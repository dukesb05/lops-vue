const steps = {
    enable_wsl: {
        text: 'Enable "Windows Subsystem for Linux" Windows Feature',
        done: false,
        running: false,
        btnText: 'Enable',
        btnRunningText: 'Enabling',
        btnDoneText: 'Enabled',
        handleFunction: 'enableWsl',
        confirmOpts: {
            title: "Restart Required",
            text:
                "Enabling WSL will require your computer to restart. Please save all your work in any other open programs before clicking the Enable button.",
            type: "warning",
            confirmButtonText: "Enable WSL",
            showCancelButton: true
        },
        successOpts: {
            title: "Success",
            text: "Restarting your computer to finish enabling WSL...",
            type: "success",
            showConfirmButton: false
        },
        requiresRestart: true,
        restartFailedOpts: {
            title: "Failed",
            text:
                "Failed to restart your computer. Please manually restart to finish enabling WSL.",
            type: "error"
        },
        failedOpts: {
            title: "Failed",
            text:
                "Failed to enable WSL on your computer. Please right click on the windows icon and click on Windows Powershell (Admin) then run this command 'Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Windows-Subsystem-Linux'. Make sure you choose to restart after the command finishes.",
            type: "error"
        }
    },
    install_env: {
        text: 'Install the LOPS WSL environment',
        done: false,
        running: false,
        btnText: 'Install',
        btnRunningText: 'Installing',
        btnDoneText: 'Installed',
        handleFunction: 'installEnvironment',
        confirmOpts: {
            title: "Install Environment",
            text:
                "Clicking the install button will install the LOPS WSL environment onto your computer. This may take a few minutes so please leave the program open and your computer on until you get a success message.",
            type: "info",
            confirmButtonText: "Install Environment",
            showCancelButton: true
        },
        successOpts: {
            title: "Success",
            text: "LOPS WSL environment has been successfully installed!",
            type: "success"
        },
        requiresRestart: false,
        failedOpts: {
            title: "Failed",
            text:
                "Failed to install the LOPS WSL environment. Please contact briand@d3corp.com and let him have access to your computer to take a look.",
            type: "error"
        }
    },
    install_netbeans: {
        text: 'Install Netbeans',
        done: false,
        running: false,
        btnText: 'Install',
        btnRunningText: 'Installing',
        btnDoneText: 'Installed',
        handleFunction: 'installNetbeans',
        confirmOpts: {
            title: "Install Netbeans",
            text:
                "Clicking the install button will install netbeans onto your computer.",
            type: "info",
            confirmButtonText: "Install Netbeans",
            showCancelButton: true
        },
        successOpts: {
            title: "Success",
            text: "Netbeans has been successfully installed!",
            type: "success"
        },
        requiresRestart: false,
        failedOpts: {
            title: "Failed",
            text:
                "Failed to install Netbeans. Please contact briand@d3corp.com and let him have access to your computer to take a look.",
            type: "error"
        }
    },
    install_authy: {
        text: 'Install Authy so you can connect to our VPN',
        done: false,
        running: false,
        btnText: 'Install',
        btnRunningText: 'Installing',
        btnDoneText: 'Installed',
        handleFunction: 'installAuthy',
        confirmOpts: {
            title: "Install Authy",
            text:
                "Clicking the install button will install Authy on your computer",
            type: "info",
            confirmButtonText: "Install Authy",
            showCancelButton: true
        },
        successOpts: {
            title: "Success",
            text: "Authy has been successfully installed!",
            type: "success"
        },
        requiresRestart: false,
        failedOpts: {
            title: "Failed",
            text:
                "Failed to install Authy. Please contact briand@d3corp.com and let him have access to your computer to take a look.",
            type: "error"
        }
    },
    install_open_vpn: {
        text: 'Install Open VPN so you can connect to our VPN',
        done: false,
        running: false,
        btnText: 'Install',
        btnRunningText: 'Installing',
        btnDoneText: 'Installed',
        handleFunction: 'installOpenVpn',
        confirmOpts: {
            title: "Install Open VPN",
            text:
                "Clicking the install button will install Open VPN on your computer",
            type: "info",
            confirmButtonText: "Install Open VPN",
            showCancelButton: true
        },
        successOpts: {
            title: "Success",
            text: "Open VPN has been successfully installed!",
            type: "success"
        },
        requiresRestart: false,
        failedOpts: {
            title: "Failed",
            text:
                "Failed to install Open VPN. Please contact briand@d3corp.com and let him have access to your computer to take a look.",
            type: "error"
        }
    },
    install_d3vpn_script: {
        text: 'Install D3 VPN script to auto connect to the vpn via open vpn and authy.',
        done: false,
        running: false,
        btnText: 'Install',
        btnRunningText: 'Installing',
        btnDoneText: 'Installed',
        handleFunction: 'installD3Vpn',
        confirmOpts: {
            title: "Install D3 VPN script",
            text:
                "Please upload your .ovpn config file from vpn.d3corp.com then click the Install D3 VPN Script button.",
            type: "info",
            confirmButtonText: "Install D3 VPN Script",
            showCancelButton: true,
            input: 'file',
            inputAttributes: {
                accept: '.ovpn',
                'aria-label': 'Upload your d3 open vpn config file.'
            }
        },
        successOpts: {
            title: "Success",
            text: "D3 VPN script has been successfully installed!",
            type: "success"
        },
        requiresRestart: false,
        failedOpts: {
            title: "Failed",
            text:
                "Failed to install D3 VPN script. Please contact briand@d3corp.com and let him have access to your computer to take a look.",
            type: "error"
        }
    }
};
export default steps;